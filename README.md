
Dependencies (packages):
****************************
1. numpy
2. scipy
3. sklearn
4. seaborn
5. matplotlib
6. pandas


Program:
****************************
The program is in Python3.
The program is menu driven. The menu options are:

1. Comparing different densities in original space and target space in stochastic neighbor embedding"
2. Replacing Gaussian distribution on original space and in target space by several other distributions and compare the results with tSNE –MNIST dataset"
3. EXIT


Option 1 will evaluate question 1. It will generate 5 dummy datasets using various distributions. For every dataset, it will evaluate TSNE. The plot of the same will be shown. The plot will have 3 parts.
The first part is the original dataset. The Second pane is the Probability Density Function of the dataset. The Third pane will show the dataset seperation after TSNE applied.


Option 2 will evaluate question 2. It will generate 4 distributions over the MNIST dataset. 
The distributions are:
1. Rayleigh
2. Cauchy
3. Exponential Weibull 
4. Chi Squared
For every distribution, MNIST feature and target space is transformed to fit the distribution.It will then evaluate TSNE on the transformed MNIST. The plot of the same will be shown. The plot will have 3 parts.
The first part is the original dataset. The Second pane is the Probability Density Function of the distribution. The Third pane will show the dataset seperation after TSNE applied.

Inference: Cauchy Distribution shows the best seperation.
