# -*- coding: utf-8 -*-
"""
@author: Raghuveera Kori
"""

import numpy as np
from sklearn.datasets import load_digits
from scipy.spatial.distance import pdist
from sklearn.manifold.t_sne import _joint_probabilities
from scipy import linalg
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import squareform
from sklearn.manifold import TSNE
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.datasets import make_blobs,make_moons,make_circles,make_hastie_10_2,make_gaussian_quantiles


import scipy
import scipy.stats as st


MACHINE_EPSILON = np.finfo(np.double).eps
n_components = 2
perplexity = 80



def draw_weibull():
    # fig, ax = plt.subplots(1, 1)
    c = 1.79
    mean, var, skew, kurt = st.weibull_min.stats(c, moments='mvsk')
    x = np.linspace(st.weibull_min.ppf(0.01, c),st.weibull_min.ppf(0.99, c), 100)
    yy=st.weibull_min.pdf(x, c)
    # ax.plot(x, yy,'r-', lw=5, alpha=0.6, label='weibull_min pdf')
    # plt.savefig("weibull.png")
    return x,yy
 
def draw_gextreme():
    # fig, ax = plt.subplots(1, 1)
    c = -0.1
    mean, var, skew, kurt = st.genextreme.stats(c, moments='mvsk')
    x = np.linspace(st.genextreme.ppf(0.01, c),st.genextreme.ppf(0.99, c), 100)
    yy=st.genextreme.pdf(x, c)
    # ax.plot(x, yy,'r-', lw=5, alpha=0.6, label='genextreme pdf')
    # plt.savefig("genextreme.png")
    return x,yy

def draw_exponweib():
    # fig, ax = plt.subplots(1, 1)
    a= 2.89
    c=1.95
    mean, var, skew, kurt = st.exponweib.stats(a, c, moments='mvsk')
    x = np.linspace(st.exponweib.ppf(0.01, a, c),st.exponweib.ppf(0.99, a, c), 100)
    yy=st.exponweib.pdf(x, a, c)
    # ax.plot(x, yy,'r-', lw=5, alpha=0.6, label='exponweib pdf')
    # plt.savefig("exponweib.png")
    return x,yy

def draw_norm():
    # fig, ax = plt.subplots(1, 1)
    a= 2.89
    c=1.95
    mean, var, skew, kurt = st.norm.stats(moments='mvsk')
    x = np.linspace(st.norm.ppf(0.01),st.norm.ppf(0.99), 100)
    yy=st.norm.pdf(x)
    # ax.plot(x, yy,'r-', lw=5, alpha=0.6, label='norm pdf')
    # plt.savefig("norm.png")
    return x,yy
    
def get_best_distribution(data):
    dist_names = ["norm", "exponweib", "weibull_max", "weibull_min", "pareto", "genextreme"]
    dist_results = []
    params = {}
    for dist_name in dist_names:
        try:
            dist = getattr(st, dist_name)
            param = dist.fit(data)
    
            params[dist_name] = param
            # Applying the Kolmogorov-Smirnov test
            D, p = st.kstest(data, dist_name, args=param)
            # print("p value for "+dist_name+" = "+str(p))
            dist_results.append((dist_name, p))
        except:
            pass

    # select the best fitted distribution
    best_dist, best_p = (max(dist_results, key=lambda item: item[1]))
    # store the name of the best fit and its p value

    # print("Best fitting distribution: "+str(best_dist))
    # print("Best p value: "+ str(best_p))
    # print("Parameters for the best fit: "+ str(params[best_dist]))

    return best_dist, best_p, params[best_dist]


def fit(X):
    n_samples = X.shape[0]
    
    # Compute euclidean distance
    distances = pairwise_distances(X, metric='euclidean', squared=True)
    
    # Compute joint probabilities p_ij from distances.
    P = _joint_probabilities(distances=distances, desired_perplexity=perplexity, verbose=False)
    
    # The embedding is initialized with iid samples from Gaussians with standard deviation 1e-4.
    X_embedded = 1e-4 * np.random.mtrand._rand.randn(n_samples, n_components).astype(np.float32)
    
    # degrees_of_freedom = n_components - 1 comes from
    # "Learning a Parametric Embedding by Preserving Local Structure"
    # Laurens van der Maaten, 2009.
    degrees_of_freedom = max(n_components - 1, 1)
    
    return _tsne(P, degrees_of_freedom, n_samples, X_embedded=X_embedded)

def _tsne(P, degrees_of_freedom, n_samples, X_embedded):
    params = X_embedded.ravel()
    
    obj_func = _kl_divergence
    
    params = _gradient_descent(obj_func, params, [P, degrees_of_freedom, n_samples, n_components])
        
    X_embedded = params.reshape(n_samples, n_components)
    return X_embedded

def _kl_divergence(params, P, degrees_of_freedom, n_samples, n_components):
    X_embedded = params.reshape(n_samples, n_components)
    
    dist = pdist(X_embedded, "sqeuclidean")
    dist /= degrees_of_freedom
    dist += 1.
    dist **= (degrees_of_freedom + 1.0) / -2.0
    Q = np.maximum(dist / (2.0 * np.sum(dist)), MACHINE_EPSILON)
    
    # Kullback-Leibler divergence of P and Q
    kl_divergence = 2.0 * np.dot(P, np.log(np.maximum(P, MACHINE_EPSILON) / Q))
    
    # Gradient: dC/dY
    grad = np.ndarray((n_samples, n_components), dtype=params.dtype)
    PQd = squareform((P - Q) * dist)
    for i in range(n_samples):
        grad[i] = np.dot(np.ravel(PQd[i], order='K'),
                         X_embedded[i] - X_embedded)
    grad = grad.ravel()
    c = 2.0 * (degrees_of_freedom + 1.0) / degrees_of_freedom
    grad *= c
    return kl_divergence, grad

def _gradient_descent(obj_func, p0, args, it=0, n_iter=50,
                      n_iter_check=1, n_iter_without_progress=300,
                      momentum=0.8, learning_rate=200.0, min_gain=0.01,
                      min_grad_norm=1e-12):

    p = p0.copy().ravel()
    update = np.zeros_like(p)
    gains = np.ones_like(p)
    error = np.finfo(np.float).max
    best_error = np.finfo(np.float).max
    best_iter = i = it

    for i in range(it, n_iter):
        error, grad = obj_func(p, *args)
        grad_norm = linalg.norm(grad)
        inc = update * grad < 0.0
        dec = np.invert(inc)
        gains[inc] += 0.2
        gains[dec] *= 0.8
        np.clip(gains, min_gain, np.inf, out=gains)
        grad *= gains
        update = momentum * update - learning_rate * grad
        p += update
        print("[t-SNE] Iteration %d: error = %.7f,"
                              " gradient norm = %.7f"
                              % (i + 1, error, grad_norm))

        if error < best_error:
                best_error = error
                best_iter = i
        elif i - best_iter > n_iter_without_progress:
            break

        if grad_norm <= min_grad_norm:
            break
    return p



def q1():

        
    for i in range(5):
        fig, (ax1, ax2,ax3) = plt.subplots(1, 3)
        fig.set_size_inches(18.5, 5.5)
        fig.suptitle('BEfore and After TSNE (Different Distributions')
        
        if i==0:
            X, y = make_blobs(n_samples=100, centers=3, n_features=13,random_state=66)  ##norm
            x,yy=draw_norm()
        if i==1:
            X, y = make_moons(n_samples=100, noise=0.1,random_state=666) ##gen-extreme
            x,yy=draw_gextreme()
        if i==2:
            X, y = make_circles(n_samples=100, noise=0.05,random_state=666) ## exponweib
            x,yy=draw_exponweib()
        if i==3:
            X, y = make_hastie_10_2(n_samples=100,random_state=6) ##weibull
            x,yy=draw_weibull()
        if i==4:
            X, y = make_gaussian_quantiles(mean=None, cov=1.0, n_samples=240, n_features=13, n_classes=2, shuffle=True, random_state=6)
            x,yy=draw_norm()
            
            



        ax2.plot(x, yy,'r-', lw=5, alpha=0.6, label='Normal Distribution pdf')
    
        classes=len(np.unique(y))
        
        data=X[:,0]
        best_dist, best_p, prms=get_best_distribution(data)
        print("Best fitting distribution: "+str(best_dist))
        print(prms)
        

        
        # sns.set(rc={'figure.figsize':(11.7,8.27)})
        palette = sns.color_palette("bright", classes)
        sns.scatterplot(X[:,0], X[:,1], hue=y, legend='full', palette=palette,ax=ax1)
        # plt.savefig("original.png")
        
        
        X_embedded = fit(X)
        
        data=X_embedded[:,0]
        best_dist, best_p, prms=get_best_distribution(data)
        print("Best fitting distribution: "+str(best_dist))

        ax1.title.set_text('Dataset--Original')
        ax2.title.set_text('PDF-Original : '+str(best_dist))
        ax3.title.set_text('Dataset-After TSNE')
        
        palette = sns.color_palette("bright", classes)
        sns.scatterplot(X_embedded[:,0], X_embedded[:,1], hue=y, legend='full', palette=palette,ax=ax3)
        plt.show()
 

def q2():

    # X, y = load_digits(return_X_y=True)   
    for i in range(4):
        fig, (ax1, ax2,ax3) = plt.subplots(1, 3)
        fig.set_size_inches(18.5, 5.5)
        fig.suptitle('MNIST data after Distribution Fitting')
        
        if i==0:
            X, y = load_digits(return_X_y=True)  
            samp = st.rayleigh.rvs(loc=5,scale=2,size=150) # samples generation
            param = st.rayleigh.fit(samp) # distribution fitting
            
            x = X
            # fitted distribution
            pdf_fitted = st.rayleigh.pdf(x,loc=param[0],scale=param[1])
            X=pdf_fitted
            
            name="Raylegh Distribution"
            x = np.linspace(st.rayleigh.ppf(0.01),st.rayleigh.ppf(0.99), 100)
            ax2.plot(x, st.rayleigh.pdf(x),'r-', lw=5, alpha=0.6, label='rayleigh pdf')
            
        if i==1:
            X, y = load_digits(return_X_y=True)  
            samp = st.cauchy.rvs(loc=0,scale=1,size=150) # samples generation
            param = st.cauchy.fit(samp) # distribution fitting
            
            x = X
            # fitted distribution
            pdf_fitted = st.cauchy.pdf(x,loc=param[0],scale=param[1])
            X=pdf_fitted
            
            name="Cauchy Distribution"
            x = np.linspace(st.cauchy.ppf(0.01),st.cauchy.ppf(0.99), 100)
            ax2.plot(x, st.cauchy.pdf(x),'r-', lw=5, alpha=0.6, label='cauchy pdf')

            
  
        if i==2:
            X, y = load_digits(return_X_y=True)  
            samp = st.exponweib.rvs(c=1.79,a=2.89, size=150) # samples generation
            param = st.exponweib.fit(samp) # distribution fitting
            
            x = X
            # fitted distribution
            pdf_fitted = st.exponweib.pdf(x,a=2.89,c=1.79,loc=param[0],scale=param[1])
            X=pdf_fitted
            
            name="Exponential Weibull"
            a=2.89
            c=1.79
            x = np.linspace(st.exponweib.ppf(0.01, a, c),st.exponweib.ppf(0.99, a, c), 100)
            yy=st.exponweib.pdf(x, a, c)
            ax2.plot(x, yy,'r-', lw=5, alpha=0.6, label='exponweib pdf')
      

            
        if i==3:
            X, y = load_digits(return_X_y=True)  
            samp = st.chi2.rvs(df=55,loc=5,scale=2,size=150) # samples generation
            param = st.genextreme.fit(samp) # distribution fitting
            
            x = X
            # fitted distribution
            pdf_fitted = st.chi2.pdf(x,df=55,loc=param[0],scale=param[1])
            X=pdf_fitted
            
            name="Chi-Square Distribution"
            df=55
            x = np.linspace(st.chi2.ppf(0.01, df),st.chi2.ppf(0.99, df), 100)
            ax2.plot(x, st.chi2.pdf(x, df),'r-', lw=5, alpha=0.6, label='chi2 pdf')

        if i==4:
            X, y = load_digits(return_X_y=True)  
            name="Normal Distribution"
            x = np.linspace(st.norm.ppf(0.01),st.norm.ppf(0.99), 100)
            yy=st.norm.pdf(x)
            ax2.plot(x, yy,'r-', lw=5, alpha=0.6, label='norm pdf')

            
        

        # ax2.plot(x, yy,'r-', lw=5, alpha=0.6, label='Normal Distribution pdf')
    
        classes=len(np.unique(y))
        
        data=X[:,0]
        best_dist, best_p, prms=get_best_distribution(data)
        print("Best fitting distribution: "+str(best_dist))
        print(prms)
        

        
        # sns.set(rc={'figure.figsize':(11.7,8.27)})
        palette = sns.color_palette("bright", classes)
        sns.scatterplot(X[:,0], X[:,1], hue=y, legend='full', palette=palette,ax=ax1)
        # plt.savefig("original.png")
        
        
        X_embedded = fit(X)
        
        data=X_embedded[:,0]
        best_dist, best_p, prms=get_best_distribution(data)
        print("Best fitting distribution: "+str(best_dist))

        ax1.title.set_text('Dataset--Original')
        ax2.title.set_text('PDF of Fitting Distribution : '+str(name))
        ax3.title.set_text('Dataset-After TSNE')
        
        palette = sns.color_palette("bright", classes)
        sns.scatterplot(X_embedded[:,0], X_embedded[:,1], hue=y, legend='full', palette=palette,ax=ax3)
        plt.show()
     
    print("_________________________________________________________")
    print("Best Data Seperation by Cauchy Distribution")
    print("_________________________________________________________")

def main():
    running=True
    while(running):
        print("________________________MENU_________________________________")
        print("1. Comparing different densities in original space and target space in stochastic neighbor embedding")
        print("2. Replacing Gaussian distribution on original space and in target space by several other distributions and compare the results with tSNE –MNIST dataset")
        print("3. EXIT")
        print()
        inp=int(input("Please enter your choice : "))
        if inp>10 or inp<1:
            print("Not a valid choice!!Please Enter Again.")
            running=True
        else:
            if inp==1:
                q1()
            if inp==2:
                q2()
            if inp==3:
                running=False 
            
            
if __name__=="__main__": 
    main()             # call main function 
            


